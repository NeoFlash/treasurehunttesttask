﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CompleteProject
{
    public class GameSaveManager : Singleton<GameSaveManager>
    {
        protected GameSaveManager() { } // guarantee this will be always a singleton only

        // gather all data and store them
        public void SaveGame()
        {
            // player
            Transform player = GameObject.FindGameObjectWithTag("Player").transform;
            Vector3Serializer playerPosition = new Vector3Serializer();
            playerPosition.Fill(player.position);
            DataStorage.Set("playerPosition", DataStorage.UniversalSerialize(playerPosition));
            QuaternionSerializer playerRotation = new QuaternionSerializer();
            playerRotation.Fill(player.rotation);
            DataStorage.Set("playerRotation", DataStorage.UniversalSerialize(playerRotation));
            DataStorage.Set("playerHealth", player.GetComponent<PlayerHealth>().currentHealth);

            // score
            DataStorage.Set("playerScore", ScoreManager.score);

            // enemies
            List<String> enemies = new List<String>();
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                EnemyHealth enemyHealthComponent = enemy.GetComponent<EnemyHealth>();
                if (enemyHealthComponent && enemyHealthComponent.currentHealth > 0)
                {
                    EnemySaveData enemyData = new EnemySaveData();
                    enemyData.prefab = enemy.GetComponent<EnemyData>().prefab;
                    enemyData.health = enemyHealthComponent.currentHealth;
                    enemyData.position = new Vector3Serializer();
                    enemyData.position.Fill(enemy.transform.position);
                    enemyData.rotation = new QuaternionSerializer();
                    enemyData.rotation.Fill(enemy.transform.rotation);
                    enemies.Add(DataStorage.UniversalSerialize(enemyData));
                }
            }
            DataStorage.SetArray("enemies", enemies.ToArray());

            // saved game marker
            DataStorage.Set("saveAvailable", true);

            // save all data
            DataStorage.Save();
        }

        public void LoadGame()
        {
            // check saved game marker
            if (!checkSavedGameMarker())
            {
                // no saved data, exit loading
                return;
            }

            // move player
            Transform player = GameObject.FindGameObjectWithTag("Player").transform;
            player.position = DataStorage.UniversalDeserialize<Vector3Serializer>(DataStorage.Get<string>("playerPosition")).V3;
            player.rotation = DataStorage.UniversalDeserialize<QuaternionSerializer>(DataStorage.Get<string>("playerRotation")).Q;
            int playerHealth = DataStorage.Get<int>("playerHealth");
            PlayerHealth playerHealthComponent = player.GetComponent<PlayerHealth>();
            playerHealthComponent.currentHealth = playerHealth;
            playerHealthComponent.healthSlider.value = playerHealth;

            // restore score
            ScoreManager.score = DataStorage.Get<int>("playerScore");

            // destroy all current enemies
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(enemy);
            }

            // instantiate enemies from saved data
            string[] enemies = DataStorage.GetArray("enemies", new string[0]);
            if (enemies.Length > 0)
            {
                EnemiesList _enemiesList = FindObjectOfType<EnemiesList>();
                foreach (string enemyDataBase64 in enemies)
                {
                    EnemySaveData enemyData = DataStorage.UniversalDeserialize<EnemySaveData>(enemyDataBase64);
                    GameObject loadedEnemy = Instantiate(_enemiesList.GetEnemyPrefab(enemyData.prefab),
                        enemyData.position.V3, enemyData.rotation.Q);
                    loadedEnemy.GetComponent<EnemyHealth>().currentHealth = enemyData.health;
                }
            }
        }

        public bool checkSavedGameMarker()
        {
            return DataStorage.Get("saveAvailable", false);
        }
    }
}

[Serializable]
public struct EnemySaveData
{
    public EnemiesList.Enemies prefab;
    public int health;
    public Vector3Serializer position;
    public QuaternionSerializer rotation;
}

[Serializable]
public struct Vector3Serializer
{
    public float x;
    public float y;
    public float z;

    public void Fill(Vector3 v3)
    {
        x = v3.x;
        y = v3.y;
        z = v3.z;
    }

    public Vector3 V3
    { get { return new Vector3(x, y, z); } }
}

[Serializable]
public struct QuaternionSerializer
{
    public float x;
    public float y;
    public float z;
    public float w;

    public void Fill(Quaternion q)
    {
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
    }

    public Quaternion Q
    { get { return new Quaternion(x, y, z, w); } }
}