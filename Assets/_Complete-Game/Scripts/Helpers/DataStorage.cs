﻿/*
 * Author: Alex Novichenko
 * Created: 2018.01.30
 * 
 * This simple data storage class can handle secure storing of IConvertible variable types.
 * Loading of data goes silently on first access to this static class. All data in it is available
 * in all scenes and is consistant between scenes changes.
 * 
 * You can save data by calling Save method any time or setting "instantSave" to true in any Set call.
 * If you need to save multiple variables at once, it's much faster to assign all variables and only then call Save.
 * 
 * It is also not recommended saving data while dynamic gameplay is in action - file IO operation can cause some additional lags.
 * Better to Set needed data and call Save method only after level is over.
 * 
 * You can get/set IConvertible variable types by using Get and Set methods. 
 * For arrays use GetArray and SetArray. Your arrays may be of all IConvertible types.
 * 
 * It is recommended to provide default values for all Get calls. If security check fails, variable will be set to this value.
 */

#define STORAGE_FILE // STORAGE_FILE or STORAGE_PLAYERPREFS can be used for storing data

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class DataStorage {
    private static SaveGameData data = null;
    private static float lastSaveTime = 0f;

    const string SAVE_FILENAME = "savedata.dat";
    const string SECRET_KEY = "jybGs6shOnHdshgjfHhfasiHgd7NtaXe";
    const Char ARRAY_SEPARATOR = (char)254;

    private static void SetNewGameData()
    {
        // TODO: here we assign all needed variables for new save
    }

    public static void ClearData()
    {
        data = new SaveGameData();
        SetNewGameData();
        Save();
    }

    #region Getters
    public static T Get<T>(string name, T defaultValue = default(T)) where T : IConvertible
    {
        CheckDataLoaded();
        if (data.stringData.ContainsKey(name) && CheckHashVal(name, data.stringData[name]))
        {
            return UniversalConverter<string, T>(data.stringData[name]);
        }
        else
        {
            return defaultValue;
        }
    }

    public static T[] GetArray<T>(string name, T[] defaultValue = null) where T : IConvertible
    {
        CheckDataLoaded();
        if (data.stringData.ContainsKey(name) && CheckHashVal(name, data.stringData[name]))
        {
            string[] stringArr = data.stringData[name].Split(ARRAY_SEPARATOR);
            return Array.ConvertAll(stringArr, new Converter<string, T>(UniversalConverter<string, T>));
        }
        else
        {
            return defaultValue;
        }
    }
    #endregion

    #region Setters
    public static void Set<T>(string name, T val, bool instantSave = false) where T : IConvertible
    {
        CheckDataLoaded();
        string newVal = UniversalConverter<T, string>(val);
        if (!data.stringData.ContainsKey(name))
        {
            data.stringData.Add(name, newVal);
        }
        else
        {
            data.stringData[name] = newVal;
        }

        SaveHashValue(name, newVal);

        if (instantSave)
        {
            Save();
        }
    }

    public static void SetArray<T>(string name, T[] val, bool instantSave = false) where T : IConvertible
    {
        string[] stringArray = Array.ConvertAll(val, new Converter<T, string>(UniversalConverter<T, string>));
        string newVal = String.Join(ARRAY_SEPARATOR.ToString(), stringArray);
        Set(name, newVal, instantSave);
    }
    #endregion

    #region Save and load data
    private static void CheckDataLoaded()
    {
        if (data == null)
        {
            Load();
        }
    }

    public static void Save()
    {
        data.playedTimeMarker += TimeSpan.FromSeconds(Time.unscaledTime - lastSaveTime);
        lastSaveTime = Time.unscaledTime;

        #if (STORAGE_PLAYERPREFS)
            PlayerPrefs.SetString("GameSaveData", UniversalSerialize(data));
        #endif

        #if (STORAGE_FILE)
        BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/" + SAVE_FILENAME);
            bf.Serialize(file, data);
            file.Close();
        #endif
    }

    public static void Load()
    {
        #if (STORAGE_PLAYERPREFS)
            string dataBase64 = PlayerPrefs.GetString("GameSaveData", "none");
            if (dataBase64 == "none")
            {
                ClearData();
            }
            else
            {
                data = UniversalDeserialize<SaveGameData>(dataBase64);
            }
        #endif

        #if (STORAGE_FILE)
            if (File.Exists(Application.persistentDataPath + "/" + SAVE_FILENAME))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/" + SAVE_FILENAME, FileMode.Open);
                data = (SaveGameData)bf.Deserialize(file);
                file.Close();
            }
            else
            {
                ClearData();
            }
        #endif
    }
#endregion

    #region Security
    private static string GetValueHash(string value)
    {
        System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(value + SECRET_KEY);
        byte[] hash = md5.ComputeHash(inputBytes);

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }

    private static void SaveHashValue(string name, string val)
    {
        if (!data.securityHashes.ContainsKey(name))
        {
            data.securityHashes.Add(name, GetValueHash(val));
        }
        else
        {
            data.securityHashes[name] = GetValueHash(val);
        }
    }

    private static bool CheckHashVal(string name, string val)
    {
        if (!data.securityHashes.ContainsKey(name))
        {
            return false;
        }
        return GetValueHash(val) == data.securityHashes[name];
    }
    #endregion

    #region Convert types methods
    public static T2 UniversalConverter<T1, T2>(T1 val) where T1 : IConvertible
                                                        where T2 : IConvertible
    {
        if (typeof(T2).IsEnum)
        {
            return (T2)Enum.Parse(typeof(T2), val.ToString());
        }
        else
        {
            return (T2)Convert.ChangeType(val, typeof(T2));
        }
    }

    public static string UniversalSerialize<T>(T data)
    {
        BinaryFormatter bf = new BinaryFormatter();
        var memoryStream = new MemoryStream();
        using (memoryStream)
        {
            bf.Serialize(memoryStream, data);
        }
        string converted = Convert.ToBase64String(memoryStream.ToArray());
        memoryStream.Close();
        return converted;
    }

    public static T UniversalDeserialize<T>(string dataBase64)
    {
        byte[] byteData = Convert.FromBase64String(dataBase64);
        using (MemoryStream stream = new MemoryStream(byteData))
        {
            BinaryFormatter bf = new BinaryFormatter();
            return (T)bf.Deserialize(stream);
        }
    }
    #endregion
}

[Serializable]
class SaveGameData
{
    public TimeSpan playedTimeMarker = TimeSpan.FromSeconds(0);
    public Dictionary<string, string> stringData = new Dictionary<string, string>();
    public Dictionary<string, string> securityHashes = new Dictionary<string, string>();
}