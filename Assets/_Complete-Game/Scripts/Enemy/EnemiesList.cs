﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesList : MonoBehaviour {
    public enum Enemies
    {
        Hellephant,
        ZomBear,
        ZomBunny
    }

    public List<GameObject> enemiesPrefabs;

    private Dictionary<Enemies, GameObject> _enemiesPrefabs;

    void Awake()
    {
        _enemiesPrefabs = new Dictionary<Enemies, GameObject>();
        foreach (GameObject enemy in enemiesPrefabs)
        {
            if (Enum.IsDefined(typeof(Enemies), enemy.name))
            {
                _enemiesPrefabs.Add((Enemies)Enum.Parse(typeof(Enemies), enemy.name), enemy);
            }
        }
    }

    public GameObject GetEnemyPrefab(Enemies enemy)
    {
        if (_enemiesPrefabs.ContainsKey(enemy))
        {
            return _enemiesPrefabs[enemy];
        }
        return null;
    }
}
